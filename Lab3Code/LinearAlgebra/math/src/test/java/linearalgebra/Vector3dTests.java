/**
 * Vector3dTest is a test class of Vector3d
 * It is inside the linearalgebra package
 * @author Shuya Liu
 * @StudentID 2136141
 * @version 9/8/2022
*/
package linearalgebra;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class Vector3dTests {
    @Test
    public void testCreateVector(){
        Vector3d vector3d = new Vector3d(1, 2, 3);
        assertTrue(vector3d.getX() == 1);
        assertTrue(vector3d.getY() == 2);
        assertTrue(vector3d.getZ() == 3);
    } 

    @Test
    public void testMagnitude(){
        Vector3d vector3d = new Vector3d(1, 2, 3);
        assertTrue(vector3d.magnitude() == Math.sqrt(14));
    }

    @Test
    public void testDotProduct(){
        Vector3d vector3d1 = new Vector3d(1, 2, 3);
        Vector3d vector3d2 = new Vector3d(3, 2, 1);
        assertTrue(vector3d1.dotProduct(vector3d2) == 10);
    }

    @Test
    public void testAdd(){
        Vector3d vector3d1 = new Vector3d(1, 2, 3);
        Vector3d vector3d2 = new Vector3d(4, 5, 6);
        Vector3d result = vector3d1.add(vector3d2); 
        assertTrue(result.getX() == 5);
        assertTrue(result.getY() == 7);
        assertTrue(result.getZ() == 9);
    }

}
