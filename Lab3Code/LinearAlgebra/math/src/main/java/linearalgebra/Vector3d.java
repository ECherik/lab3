/**
 * Vector3d is the production code
 * It is inside the linearalgebra package
 * @author Shuya Liu
 * @StudentID 2136141
 * @version 9/8/2022
*/
package linearalgebra;
public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d (double newX, double newY, double newZ){
        this.x = newX;
        this.y = newY;
        this.z = newZ;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        double result = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
        return result;
    }

    public double dotProduct(Vector3d newVector3d){
        double result = this.x * newVector3d.x + this.y * newVector3d.y + this.z * newVector3d.z;
        return result;
    }

    public Vector3d add(Vector3d newVector3d){
        Vector3d newVec = new Vector3d(this.x + newVector3d.x, this.y + newVector3d.y, this.z + newVector3d.z);
        return newVec;
    }

}